/*
 * Accelerator.h
 *
 *  Created on: Apr 12, 2015
 *      Author: Kaiden
 */

#ifndef ACCELERATOR_H_
#define ACCELERATOR_H_


//-- system constants ----------------------------------------------------------
#define myCLOCK		12000000				// clock speed
#define CLOCK		_12MHZ

//-- watchdog constants --------------------------------------------------------
#define WDT_CLK		32000				// 32 Khz WD clock (@1 Mhz)
#define	WDT_CTL		WDT_MDLY_32			// WDT SMCLK, ~32ms
#define	WDT_CPS		myCLOCK/WDT_CLK		// WD clocks / second count

#if WDT_CPS/50
#define DEBOUNCE_CNT	WDT_CPS/50		// 20 ms debounce count
#else
#define DEBOUNCE_CNT	1				// 20 ms debounce count
#endif

//-- sys_events ----------------------------------------------------------------
#define SWITCH_1		0x0001
#define SWITCH_2		0x0002
#define SWITCH_3		0x0004
#define SWITCH_4		0x0008

#define INIT_SEQUENCE	0x0010
#define READ_SEQUENCE	0x0020

#endif /* ACCELERATOR_H_ */
