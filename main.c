#include <msp430.h>
#include "RBX430_i2c.h"
#include "Accelerator.h"

/*
 * main.c
 */
volatile uint16 WDT_cps_cnt;			// WDT count per second
volatile uint16 WDT_debounce_cnt;		// switch debounce counter
volatile uint32 WDT_delay;				// WDT delay counter
volatile uint16 sys_event;				// pending events

uint8 adxl345_int;

void port1_init(void);					// initialize switches
void watchdogtimer_init(uint16);		// initialize watchdog

int main(void) {
    ADXL345_init();
	port1_init();						// init port 1 (switches)
	watchdogtimer_init(WDT_CTL);		// init watchdog timer

	//--------------------------------------------------------------------------
		//	event service routine loop
		//
		while (1)
		{
			// disable interrupts before check sys_event
			_disable_interrupts();

			if (sys_event)
			{
				// if there's something pending, enable interrupts before servicing
				_enable_interrupt();
			}
			else
			{
				// otherwise, enable interrupts and goto sleep (LPM0)
				__bis_SR_register(LPM0_bits + GIE);
			}

			//----------------------------------------------------------------------
			//	I'm AWAKE!!!  There must be something to do
			//----------------------------------------------------------------------

			if (sys_event & INIT_SEQUENCE)			// move snake event
			{
				sys_event &= ~INIT_SEQUENCE;			// clear move event
			}
			else if (sys_event & READ_SEQUENCE)			// switch #1 event
			{
				sys_event &= ~READ_SEQUENCE;				// clear switch #1 event
			}
			else if (sys_event & SWITCH_1)			// switch #1 event
			{
				sys_event &= ~SWITCH_1;				// clear switch #1 event
			}
			else if (sys_event & SWITCH_2)			// switch #2 event
			{
				sys_event &= ~SWITCH_2;				// clear switch #2 event
			}
			else if (sys_event & SWITCH_3)			// switch #3 event
			{
				sys_event &= ~SWITCH_3;				// clear switch #3 event
			}
			else if (sys_event & SWITCH_4)			// switch #4 event
			{
				sys_event &= ~SWITCH_4;				// clear switch #4 event
			}
			else									// ????
			{
				ERROR2(10);							// unrecognized event
			}
		}

	/*
	lcd_cursor(40, 60);
	lcd_printf("DATAX:%d ", XYZdata.xyz.XX);

	lcd_cursor(40, 52);
	lcd_printf("DATAY:%d ", XYZdata.xyz.YY);

	lcd_cursor(40, 44);
	lcd_printf("DATAZ:%d ", XYZdata.xyz.ZZ);
	 */
    ADXL345_read(XL345_INT_SOURCE, &adxl345_int, 1);

	return 0;
}
//-- port1_init ----------------------------------------------------------------
//
void port1_init(void)
{
	// configure P1 switches and ADXL345 INT1 for interrupt
	P1SEL &= ~0x0f;						// select GPIO
	P1DIR &= ~0x0f;						// Configure P1.0-3 as Inputs
	P1OUT |= 0x0f;						// use pull-ups
	P1IES |= 0x0f;						// high to low transition
	P1REN |= 0x0f;						// Enable pull-ups
	P1IE |= 0x0f;						// P1.0-3 interrupt enabled
	P1IFG &= ~0x0f;						// P1.0-3 IFG cleared
	return;
} // end port1_init
//-- watchdogtimer_init --------------------------------------------------------
//
void watchdogtimer_init(uint16 wd_ctl)
{
	WDTCTL = wd_ctl;					// set Watchdog interval
	IE1 |= WDTIE;						// enable WDT interrupt
	WDT_cps_cnt = WDT_CPS;				// set WD 1 second counter
	WDT_debounce_cnt = 0;				// switch debounce
	return;
} // end watchdogtimer_init

//-- Watchdog Timer ISR --------------------------------------------------------
//
#pragma vector = WDT_VECTOR
__interrupt void WDT_ISR(void)
{
	if (--WDT_cps_cnt == 0)					// 1 second counter
	{
		LED_GREEN_TOGGLE;					// toggle GREEN led
		WDT_cps_cnt = WDT_CPS;
	}

	// check for switch debounce
	if (WDT_debounce_cnt && (--WDT_debounce_cnt == 0))
	{
		sys_event |= (P1IN ^ 0x0f) & 0x0f;
	}

	if (sys_event) __bic_SR_register_on_exit(LPM0_bits);
	return;
} // end WDT_ISR
